angular.module('starter.controllers', ['starter.services', 'ngOpenFB'])



.controller('SignInCtrl', function($scope, $state, $ionicModal, $timeout, ngFB) {

$scope.fbLogin = function () {
    ngFB.login({scope: 'email,user_posts,publish_actions'}).then(
        function (response) {
            if (response.status === 'connected') {
                console.log('Facebook login succeeded');
              //  $scope.closeLogin();
                $state.go('tab.dash');
            } else {
                alert('Facebook login failed');
            }
        });
};

          
                var uiConfig = {
                 'signInSuccessUrl': './#/tab/dash',
                 'signInOptions': [
                 firebase.auth.FacebookAuthProvider.PROVIDER_ID,
                 firebase.auth.EmailAuthProvider.PROVIDER_ID
                 ],
                 'tosUrl': 'www.google.com',
                 };
                var app = firebase.initializeApp(config);
                 var auth = app.auth();
                 var ui = new firebaseui.auth.AuthUI(auth);

                 ui.start('#firebaseui-auth-container', uiConfig);
                 


})  




.controller('SignUpCtrl', function($scope, $state) {

    $scope.signUp = function(name, email, password) {
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        localStorage.setItem("password", password);
        $state.go('tab.dash');
        //alert(" Usuario" + " " + localStorage.getItem("name"));
    }
})

.controller('ForgotPassCtrl', function($scope, $state, $ionicLoading) {
    $scope.user = {};
    $scope.error = {};
    $scope.state = {
        success: false
    };

    $scope.reset = function() {
        $scope.loading = $ionicLoading.show({
            content: 'Sending',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
    });
};
})


.controller('PhotoCtrl', function($scope, $cordovaCamera, $ionicModal) {

    $scope.donations = [
        { name: 'Aliah Cambr',
          photo: $scope.imgURI = 'img/mouse.jpg' }
    ];

    $scope.createDonation = function(donationItem) {
        console.log("Entrou no create");
        $scope.donations.push({
            item: donationItem.item,
            desc: donationItem.desc,
            photo: $scope.imgURI
        });
        console.log("Item:", donations.item);
        console.log("Desc:", donationItem.desc);
        $scope.modal.hide();
    };

    $scope.takePhoto = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
            $scope.modal.show();

        }, function(err) {
            // An error occured. Show a message to the user
            console.log("Error taking image", err);
        });
    }

    $scope.choosePhoto = function() {
        var options = {
            quality: 75,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
            $scope.modal.show();

        }, function(err) {
            console.log("Error getting image", err);
        });
    }


    $ionicModal.fromTemplateUrl('templates/modalDonationSettings.html', {
        scope: $scope,
        animation: 'slide-in-up',
        focusFirstInput: true
    }).then(function(modal) {
        $scope.modal = modal;

    });

    $scope.openModal = function() {
        $scope.modal.show();
    };

    $scope.closeModal = function() {
        $scope.modal.hide();
    };


    $scope.$on('$destroy', function() {
        $scope.modal.remove();
    });

    $scope.$on('modal.hidden', function() {
        // Execute action
    });

    $scope.$on('modal.removed', function() {
        // Execute action
    });

})


.controller('DashCtrl', function($scope, $state) {

    $scope.account = function(user) {
        $state.go('tab.account');
    };

})


.controller('ImageController', function($scope, $cordovaDevice, $cordovaFile, $ionicPlatform, $ionicActionSheet, ImageService, FileService) {
 
function CheckScopeBeforeApply() {
    if(!$scope.$$phase) {
         $scope.$apply();
    }
  };

  $ionicPlatform.ready(function() {
    $scope.images = FileService.images();
    CheckScopeBeforeApply();
  });
 
  $scope.urlForImage = function(imageName) {
    var trueOrigin = cordova.file.dataDirectory + imageName;
    return trueOrigin;
  }
 
  $scope.addMedia = function() {
    $scope.hideSheet = $ionicActionSheet.show({
      buttons: [
        { text: 'Take photo' },
        { text: 'Photo from library' }
      ],
      titleText: 'Add images',
      cancelText: 'Cancel',
      buttonClicked: function(index) {
        $scope.addImage(index);
      }
    });
  }
 
  $scope.addImage = function(type) {
    $scope.hideSheet();
    ImageService.handleMediaDialog(type).then(function() {
      CheckScopeBeforeApply();
    });


  


  }  
  
})

.controller('ChatsCtrl', function($scope, Chats) {
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.chats = Chats.all();
    $scope.remove = function(chat) {
        Chats.remove(chat);
    };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
    $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $state) {

    

    $scope.settings = {
        enableFriends: true
    };
});